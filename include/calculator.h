#ifndef calculator_hpp
#define calculator_hpp

#include <iostream>
#include <sstream>
#include <map>
#include <cmath>
#include <functional>
#include <stdexcept>

class Calculator
{
public:
    Calculator() {}
    float calculation(std::string &buffer);
    
protected:
    float addition(const std::pair<float, float> &numbers);
    float subtraction(const std::pair<float, float> &numbers);
    float multiplication(const std::pair<float, float> &numbers);
    float division(const std::pair<float, float> &numbers);
    float toNumber(const std::string &number);
    float operations(const std::pair<std::string, std::string> &numbers, const char operation);
    float readBuffer(std::string &buffer);
    
private:
    int recursionCounter_ = 0;
    float result_ = 0.0;
//    const std::map<char, std::function<float(const std::pair<float, float>&)>> operations_ =
//    {
//        {'+', [](auto &pair_) { return pair_.first + pair_.second;}}, // Сложение
//        {'-', [](auto &pair_) { return pair_.first - pair_.second;}}, // Вычитание
//        {'*', [](auto &pair_) { return pair_.first * pair_.second;}}, // Умножение
//        {'/', [](auto &pair_) { return pair_.first / pair_.second;}}, // Деление
//    };
};

#endif /* calculator_h */
