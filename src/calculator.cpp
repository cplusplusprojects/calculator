#include "calculator.h"

float Calculator::calculation(std::string &buffer)
{
    recursionCounter_ = 0;
    result_ = readBuffer(buffer);
    result_ == 0 ? result_ = 0 : result_;
    return round(result_ * 100) / 100;
}

// Сложение
float Calculator::addition(const std::pair<float, float> &numbers)
{
    return numbers.first + numbers.second;
}

// Вычитание
float Calculator::subtraction(const std::pair<float, float> &numbers)
{
    return numbers.first - numbers.second;
}

// Умножение
float Calculator::multiplication(const std::pair<float, float> &numbers)
{
    return numbers.first * numbers.second;
}

// Деление
float Calculator::division(const std::pair<float, float> &numbers)
{
    const float divisor = numbers.second;
    if (divisor == 0)
        throw std::invalid_argument("некорректный ввод, делитель не может быть 0");
    return numbers.first / numbers.second;
}

// Конвертация в число
float Calculator::toNumber(const std::string &number)
{
    return round(std::stof(number) * 100) / 100;
}

// Выбор действия
float Calculator::operations(const std::pair<std::string, std::string> &numbersString, const char operation)
{
    float fisrtNumber = toNumber(numbersString.first);
    float secondNumber = toNumber(numbersString.second);
    switch (operation)
    {
        case '+':
            return addition({fisrtNumber, secondNumber});

        case '-':
            return subtraction({fisrtNumber, secondNumber});

        case '*':
            return multiplication({fisrtNumber, secondNumber});

        case '/':
            return division({fisrtNumber, secondNumber});

        default:
            throw std::invalid_argument((char*)&operation);
    }
//    auto found = operations_.find(operation);
//    if (found != operations_.end())
//    {
//        auto _operation = found->second;
//        return _operation({fisrtNumber, secondNumber});
//    }
//    else
//    {
//        throw std::invalid_argument((char*)&operation);
//    }
    return 0;
}

// Рекурсивный счет строки
float Calculator::readBuffer(std::string &buffer)
{
    std::string first, second;
    std::string input;
    char operation = '\0', prevOperation = '\0';
    float result = 0.0;
    int sign = 1;
    int size = static_cast<int>(buffer.size()) - 1;
    for (int i = 0; i <= size; ++i)
    {
        char c = buffer[i];
        if (c == ' ')
        {
            continue;
        }
        else if (c == '-' && i == 0 && input.empty())
        {
            sign = -1;
        }
        else if (isdigit(c))
        {
            if (sign == -1)
            {
                input += "-";
                sign = 1;
            }
            input += c;
        }
        else if (c == '.' || c == ',')
        {
            input += '.';
        }
        else if (c == '+' || c == '-' || c == '*' || c == '/')
        {
            if (input.empty())
            {
                throw std::invalid_argument((char*)&c);
            }
            else if (operation == '\0')
            {
                first = input;
                input.clear();
            }
            else if ((c == '*' || c == '/') && (operation == '+' || operation == '-'))
            {
                second = first;
                first = input;
                prevOperation = operation;
            }
            else if (prevOperation != '\0')
            {
                result = operations({first, input}, operation);
                result = operations({second, std::to_string(result)}, prevOperation);
                first = std::to_string(result);
                second.clear();
                prevOperation = '\0';
            }
            else
            {
                result = operations({first, input}, operation);
                first = std::to_string(result);
            }
            operation = c;
            input.clear();
        }
        else if (c == '(')
        {
            ++recursionCounter_;
            buffer = buffer.substr(i + 1);
            input += std::to_string(sign * readBuffer(buffer));
            --recursionCounter_;
            i = -1;
            sign = 1;
            size = static_cast<int>(buffer.size());
        }
        else if (c == ')')
        {
            if (recursionCounter_ == 0)
            {
                throw std::invalid_argument(")");
            }
            buffer = buffer.substr(i + 1);
            if (first.empty())
            {
                return toNumber(input);
            }
            return operations({first, input}, operation);
        }
        else if (c != '\0')
        {
            throw std::invalid_argument((char*)&c);
        }
        if (i == size)
        {
            if (first.empty() || input.empty())
            {
                if (operation != '\0')
                {
                    throw std::invalid_argument((char*)&c);
                }
                result = std::stof(input);
            }
            else
            {
                result = operations({first, input}, operation);
                if (!second.empty() && prevOperation != '\0')
                {
                    result = operations({second, std::to_string(result)}, prevOperation);
                }
            }
        }
    }
    if (recursionCounter_ != 0)
    {
        throw std::invalid_argument("(");
    }
    return result;
}


